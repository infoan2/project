#include "Piece.h"

Piece::Piece(Piece::Body body, Piece::Color color, Piece::Height height, Piece::Shape shape)
{
	m_Body = body;
	m_Color = color;
	m_Height = height;
	m_Shape = shape;
}

Piece::Body Piece::getMBody() const
{
	return m_Body;
}

Piece::Color Piece::getMColor() const
{
	return m_Color;
}

Piece::Height Piece::getMHeight() const
{
	return m_Height;
}

Piece::Shape Piece::getMShape() const
{
	return m_Shape;
}

std::ostream &operator<<(std::ostream &os, const Piece &piece)
{
	switch (piece.m_Body)
	{
		case Piece::Body::Full:
			os << "Full ";
			break;
		case Piece::Body::Hollow:
			os << "Hollow ";
			break;
	}
	switch (piece.m_Color)
	{
		case Piece::Color::Dark:
			os << "Dark ";
			break;
		case Piece::Color::Light:
			os << "Light ";
			break;
	}
	switch (piece.m_Height)
	{
		case Piece::Height::Short:
			os << "Short ";
			break;
		case Piece::Height::Tall:
			os << "Tall ";
			break;
	}
	switch (piece.m_Shape)
	{
		case Piece::Shape::Round:
			os << "Round ";
			break;
		case Piece::Shape::Square:
			os << "Square ";
			break;
	}
	return os;
}
