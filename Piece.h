#ifndef PROJECT_PIECE_H
#define PROJECT_PIECE_H


#include <ostream>

class Piece
{
public:
	enum class Body
	{
		Full,
		Hollow
	};
	enum class Color
	{
		Dark,
		Light
	};
	enum class Height
	{
		Short,
		Tall
	};
	enum class Shape
	{
		Round,
		Square
	};

private:
	Body m_Body;
	Color m_Color;
	Height m_Height;
	Shape m_Shape;

public:
	Piece(Body body, Color color, Height height, Shape shape);
	
	Body getMBody() const;
	
	Color getMColor() const;
	
	Height getMHeight() const;
	
	Shape getMShape() const;
	
	friend std::ostream &operator<<(std::ostream &os, const Piece &piece);
};


#endif //PROJECT_PIECE_H
