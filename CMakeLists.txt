cmake_minimum_required(VERSION 3.17)
project(project)

set(CMAKE_CXX_STANDARD 20)

add_executable(project Game.cpp Piece.cpp Piece.h)