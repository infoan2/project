#include <iostream>
#include "Piece.h"

using namespace std;

int main()
{
	Piece piece(Piece::Body::Full, Piece::Color::Dark, Piece::Height::Short, Piece::Shape::Round);
	cout << piece;
	return 0;
}
